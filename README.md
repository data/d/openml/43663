# OpenML dataset: Womens-E-Commerce-Clothing-Reviews

https://www.openml.org/d/43663

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Welcome. This is a Womens Clothing E-Commerce dataset revolving around the reviews written by customers. Its nine supportive features offer a great environment to parse out the text through its multiple dimensions. Because this is real commercial data, it has been anonymized, and references to the company in the review text and body have been replaced with retailer.
Content
This dataset includes 23486 rows and 10 feature variables. Each row corresponds to a customer review, and includes the variables:

Clothing ID: Integer Categorical variable that refers to the specific piece being reviewed. 
Age: Positive Integer variable of the reviewers age.
Title: String variable for the title of the review.
Review Text: String variable for the review body. 
Rating: Positive Ordinal Integer variable for the product score granted by the customer from 1 Worst, to 5 Best. 
Recommended IND: Binary variable stating where the customer recommends the    product where 1 is recommended, 0 is not recommended.    
Positive Feedback Count: Positive Integer documenting the number of other customers who found this review positive.
Division Name: Categorical name of the product high level division.
Department Name: Categorical name of the product department name.
Class Name: Categorical name of the product class name.

Acknowledgements
Anonymous but real source
Inspiration
I look forward to come quality NLP! There is also some great opportunities for feature engineering, and multivariate analysis.
Publications
Statistical Analysis on E-Commerce Reviews, with Sentiment Classification using Bidirectional Recurrent Neural Network 
by Abien Fred Agarap - Github

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43663) of an [OpenML dataset](https://www.openml.org/d/43663). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43663/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43663/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43663/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

